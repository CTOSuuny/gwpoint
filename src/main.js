import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import Home from './views/Home'
import Reward from './views/Reward'
import GuestExperience from './views/GuestExperience'
import Limousine from './views/Limousine'
import LimousineDetail from './views/LimousineDetail'
import LimousineOtherDetail from './views/LimousineOtherDetail'
import HotelAndResort from './views/HotelAndResort'
import HotelAndResortDetail from './views/HotelAndResortDetail'
import Dining from './views/Dining'
import AvailablePointRedemption from './views/AvailablePointRedemption'
import AvailablePointRedemptionDetail from './views/AvailablePointRedemptionDetail'
import PrivilegeDiningDiscount from './views/PrivilegeDiningDiscount'
import PrivilegeDiningDiscountDetail from './views/PrivilegeDiningDiscountDetail'
import ClubAndLounge from './views/ClubAndLounge'
import ClubAndLoungeDetail from './views/ClubAndLoungeDetail'
import Shopping from './views/Shopping'
import ShoppingDetail from './views/ShoppingDetail'
import SpaAndWellness from './views/SpaAndWellness'
import RelaxingMassage from './views/RelaxingMassage'
import RelaxingMassageDetail from './views/RelaxingMassageDetail'
import MedicalClinicDetail from './views/MedicalClinicDetail'
import PrivilegeSpaDiscount from './views/PrivilegeSpaDiscount'
import PrivilegeSpaDiscountDetail from './views/PrivilegeSpaDiscountDetail'
import ExcursionsAndActivities from './views/ExcursionsAndActivities'
import ExcursionsAndActivitiesDetail from './views/ExcursionsAndActivitiesDetail'
import RewardDetail from './views/RewardDetail'
import About from './views/About'
import Tranfer from './views/Tranfer'
import TranferUser from './views/TranferUser'
import TranferDetail from './views/TranferDetail'
import TranferUserDetail from './views/TranferUserDetail'
import Topup from './views/Topup'
import BorrowPoint from './views/BorrowPoint'
import Contact from './views/Contact'
import Privacy from './views/Privacy'
import User from './views/User'
import Terms from './views/Terms'
import UserProfile from './views/UserProfile'
import PointStatement from './views/PointStatement'
import HistoryPoint from './views/HistoryPoint'
import UsingPoint from './views/UsingPoint'
import TopupHistory from './views/TopupHistory'
import InformTransfer from './views/InformTransfer'
import BorrowHistory from './views/BorrowHistory'
import TranferHistory from './views/TranferHistory'
import MessageBox from './views/MessageBox'
import ChangePassword from './views/ChangePassword'

import SignIn from './views/SignIn'
import Login from './views/Login'
import ResetPassword from './views/ResetPassword'
import Condition from './views/Condition'
import Success from './views/Success'

library.add(faSearch, faCalendarAlt)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueRouter)
const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
  { path: '/reward', component: Reward },
  { path: '/privacy', component: Privacy },
  { path: '/reward/:id', component: RewardDetail },
  { path: '/guest-experience', component: GuestExperience },
  { path: '/limousine', component: Limousine },
  { path: '/limousine-detail/:id', component: LimousineDetail },
  { path: '/limousine-other-detail/:id', component: LimousineOtherDetail },
  { path: '/hotel-resort', component: HotelAndResort },
  { path: '/hotel-resort-detail/:id', component: HotelAndResortDetail },
  { path: '/dining', component: Dining },
  { path: '/available-point-redemption', component: AvailablePointRedemption },
  { path: '/available-point-redemption-detail/:id', component: AvailablePointRedemptionDetail },
  { path: '/privilege-dining-discount', component: PrivilegeDiningDiscount },
  { path: '/privilege-dining-discount-detail/:id', component: PrivilegeDiningDiscountDetail },
  { path: '/club-lounge', component: ClubAndLounge },
  { path: '/club-lounge-detail/:id', component: ClubAndLoungeDetail },
  { path: '/shopping', component: Shopping },
  { path: '/shopping-detail/:id', component: ShoppingDetail },
  { path: '/spa-wellness', component: SpaAndWellness },
  { path: '/relaxing-massage', component: RelaxingMassage },
  { path: '/relaxing-massage-detail/:id', component: RelaxingMassageDetail },
  { path: '/medical-clinic-detail/:id', component: MedicalClinicDetail },
  { path: '/privilege-spa-discount', component: PrivilegeSpaDiscount },
  { path: '/privilege-spa-discount-detail/:id', component: PrivilegeSpaDiscountDetail },
  { path: '/excursions-activities', component: ExcursionsAndActivities },
  { path: '/excursions-activities-detail/:id', component: ExcursionsAndActivitiesDetail },
  { path: '/tranfer', component: Tranfer },
  { path: '/tranfer-user', component: TranferUser },
  { path: '/tranfer/:id', component: TranferDetail },
  { path: '/tranfer-user/:id', component: TranferUserDetail },
  { path: '/topup', component: Topup },
  { path: '/borrow-point', component: BorrowPoint },
  { path: '/contact', component: Contact },
  { path: '/privacy', component: Privacy },
  { 
    path: '/user', 
    component: User,
    children: [
      { path: '/user/profile', component: UserProfile },
      { path: '/user/terms', component: Terms },
      { path: '/user/statement', component: PointStatement },
      { path: '/user/history', component: HistoryPoint },
      { path: '/user/using', component: UsingPoint },
      { path: '/user/topup-history', component: TopupHistory },
      { path: '/user/inform-transfer', component: InformTransfer },
      { path: '/user/borrow-history', component: BorrowHistory },
      { path: '/user/tranfer-history', component: TranferHistory },
      { path: '/user/message-box', component: MessageBox },
      { path: '/user/change-password', component: ChangePassword }
    ]
  },
  {
    path: '/signin',
    component: SignIn,
    children: [
      { path: '/signin/login', component: Login },
      { path: '/signin/reset-password', component: ResetPassword },
      { path: '/signin/condition', component: Condition },
      { path: '/signin/success', component: Success }
    ]
  }
]
const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0)
  next();
  const publicPages = ['/signin/login', '/signin/reset-password', '/signin/condition', '/signin/success'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  // if (authRequired && !loggedIn) {
  //   return next('/signin/login');
  // }

  next();
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
