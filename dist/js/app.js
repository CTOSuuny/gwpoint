$(function() {
  function fixDiv() {
    var $cache = $('#navbar-scroll');
    if ($(window).scrollTop() > 100) {
      $cache.addClass('fix-h');
    }else {
      $cache.removeClass('fix-h');
    }
  }
  $(window).scroll(fixDiv);
  fixDiv();
})